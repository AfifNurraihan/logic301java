package logic04;

import java.util.Scanner;

public class Soal01_CamelCase {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.println("Input the words : ");
        String Word = input.next();
        int jumlah = 0;
        for (int i = 0; i < Word.length(); i++){
            if (Character.isUpperCase(Word.charAt(i))){
                jumlah++;
            }

        }
        System.out.print(jumlah + 1);
        System.out.println();
    }
}
