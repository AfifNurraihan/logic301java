package logic02;

import java.util.Scanner;

public class Soal04 {
    public static void Resolve() {
        try (Scanner input = new Scanner(System.in)) {
            System.out.print("Please enter the value of n: ");
            int n = input.nextInt();
            System.out.print("Please enter the value of n2: ");
            int n2 = input.nextInt();
            int helper=n2;
            int k=1;

            // Prerequisite
            int[][] array2dInteger = new int[2][n];

            for (int i = 0; i < array2dInteger.length; i++) {
                for (int j = 0; j < array2dInteger[0].length; j++) {
                    if (i == 0){
                        array2dInteger[i][j] = j;
                    }
                    else{
                        if((j+1)%2!=0) {
                            array2dInteger[i][j] = k;
                            k+=1;
                        }
                        else {
                            array2dInteger[i][j] = helper;
                            helper+=5;
                        }
                    }
                }
            }
            // Print Array
            for (int i = 0; i < array2dInteger.length; i++) {
                for (int j = 0; j < array2dInteger[0].length; j++) {
                    System.out.print(array2dInteger[i][j] + " ");
                }
                System.out.println();
            }
        }
    }

}

