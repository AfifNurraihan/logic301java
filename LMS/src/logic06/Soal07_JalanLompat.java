package logic06;

import java.util.Scanner;

public class Soal07_JalanLompat {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        //Print keterangan simbol
        System.out.println("Deskripsi simbol: ");
        System.out.println("Pola          : ");
        System.out.println("- = jalan \t\to = lubang");
        System.out.println("Cara Berjalan :");
        System.out.println("w = berjalan \tj = lompat");
        System.out.println();
        //Input pola
        System.out.print("Input pola : ");
        String pola = input.nextLine();
        char[] arrayPola = pola.toCharArray();

        //Input cara jalan
        System.out.print("Input cara jalan : ");
        String jalan = input.nextLine();
        char[] arraycarajalan = jalan.toCharArray();

        //Definisi Variabel
        int energi = 0;

        int i = 0;
        int j=0;
        while (i < arrayPola.length) { //i = Panjang ArrayPola
            while(j<arraycarajalan.length){//j = Panjang ArrayJalan
                if (arrayPola[i] == '-') { // Kondisi pola ketika '-'
                    if (arraycarajalan[j] == 'w') { //Kondisi ketika '-' == 'w'
                        energi += 1; //Kondisi jalan energi plus 1
                        if (energi <= 0) {
                            System.out.println("Jim Died"); //Kondisi mati jika energi habis
                            return;
                        }
                    } else if (arraycarajalan[j] == 'j') { //Kondisi ketika '-' == 'j'
                        energi -= 2; //Kondisi jalan energi kurang 2
                        if (energi <= 0) {
                            System.out.println("Jim Died");//Kondisi mati jika energi habis
                            return;
                        }
                        i = i + 1; //Setiap melakukan jump ('j') maka akan melakukan 2 lompatan
                    }
                } else if (arrayPola[i] == 'o') { //Kondisi pola ketika 'o'
                    if (arraycarajalan[j] == 'j') { //Kondisi ketika 'o' == 'j'
                        energi -= 2; //Kondisi jalan energi kurang 2
                        if (energi <= 0) {
                            System.out.println("Jim Died");//Kondisi mati jika energi habis
                            return;
                        }
                        i = i + 1;//Setiap melakukan jump ('j') maka akan melakukan 2 lompatan
                    }else if (arraycarajalan[j] == 'w') { //Kondisi ketika 'o' == 'w'
                        System.out.println("Jim Died");//Kondisi mati jika energi habis
                        return;
                    }
                }
                j++; //iterasi j bertambah untuk membaca next arraycarajalan
                i++;//iterasi i bertambah untuk membaca next arrayPola
            }
        }
        System.out.println("Energi Jim Saat ini : " + energi); //Output Energi Jim
    }
}