package logic06;

import java.util.Scanner;

public class Soal04_BelanjaAndi {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Input uang Andi: Rp. ");
        int jumlahUang = input.nextInt();

        System.out.print("Input jumlah barang: ");
        int jumlahBarang = input.nextInt();

        int[] hargabarang = new int[jumlahBarang];
        String[] namabarang = new String[jumlahBarang];

        for (int i = 0; i < jumlahBarang; i++) {
            System.out.print("Nama barang ke-" + (i+1) + " : ");
            input.nextLine();
            namabarang[i] = input.nextLine();
            System.out.print("Harga barang ke-" + (i+1) + " : Rp. ");
            hargabarang[i] = input.nextInt();
        }

        int total=0;
        int helper=0;
        String hasilbarang[] = new String[hargabarang.length];

        int i=0;
        int j=0;
        while(i< namabarang.length){
            total=0;
            total=hargabarang[i];
            while(j < namabarang.length){
                if(i==j){
                    continue;
                }
                else{
                    total = total+hargabarang[j];
                    if(total <= jumlahUang){
                        hasilbarang[0]=namabarang[i];
                        helper++;
                        hasilbarang[helper] = namabarang[j];
                    }else{
                        total=total-hargabarang[j];
                    }
                }
                j++;
            }
            i++;
        }
        System.out.println("Barang yang dapat dibeli : ");
        for (int k = 0; k < helper+1; k++) {
            System.out.print(hasilbarang[k]+", ");
        }
    }
}
