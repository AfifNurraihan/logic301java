package logic06;

import java.util.Scanner;

public class Soal13_PanjangTaliKecildanBesar {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        System.out.print("Masukkan panjang tali (m) : ");
        int panjangtali = input.nextInt();
        System.out.print("Panjang tali yang diinginkan (m) : ");
        int panjangtali2 = input.nextInt();
        int helper = 0;
        if (panjangtali % panjangtali2 != 0) {
            System.out.println("Tali tidak dapat dipotong sama rata");
        } else if (panjangtali == panjangtali2) {
            System.out.println("Tali tidak perlu dipotong");
        } else {
            int hasil;
            int hasil2;
            do {
                hasil = panjangtali / 2;
                hasil2 = panjangtali % 2;
                panjangtali = hasil + hasil2;
                ++helper;
            } while(panjangtali > panjangtali2);
            System.out.println("Cukup memotong "+helper +" kali untuk memperoleh panjang tali "+ panjangtali2 + " meter");
        }
    }
}


