package logic05;

import java.util.Scanner;

public class Soal05_PorsiMakan {
    public static void main(String[] args) {
        //Mendefinisikan variabel
        float jumlahLaki = 0.0F;
        float jumlahPerempuan = 0.0F;
        float jumlahRemaja = 0.0F;
        float jumlahAnak = 0.0F;
        float jumlahBalita = 0.0F;

        //Membuat pilihan kategori usia
        System.out.print("1. Laki-laki dewasa");
        System.out.print("\n2. Perempuan dewasa");
        System.out.print("\n3. Remaja");
        System.out.print("\n4. Anak-anak");
        System.out.print("\n5. Balita");

        //Membuat perulangan
        char ulangMenu;
        //Perulangan untuk memasukkan jumlah orang per usia
        do {
            System.out.print("\nPilih rentang usia (1/2/3/4/5) : ");
            Scanner input = new Scanner(System.in);
            int inputCase = input.nextInt();
            switch (inputCase) {
                case 1:
                    System.out.print("Jumlah laki-laki dewasa : ");
                    int lakiDewasa = input.nextInt();
                    jumlahLaki += (float)lakiDewasa; //Perhitungan akan diubah kebentuk float
                    break;
                case 2:
                    System.out.print("Jumlah perempuan dewasa : ");
                    int perempuanDewasa = input.nextInt();
                    jumlahPerempuan += (float)perempuanDewasa; //Perhitungan akan diubah kebentuk float
                    break;
                case 3:
                    System.out.print("Jumlah remaja : ");
                    int remaja = input.nextInt();
                    jumlahRemaja += (float)remaja; //Perhitungan akan diubah kebentuk float
                    break;
                case 4:
                    System.out.print("Jumlah anak-anak : ");
                    int anak = input.nextInt();
                    jumlahAnak += (float)anak; //Perhitungan akan diubah kebentuk float
                    break;
                case 5:
                    System.out.print("Jumlah balita : ");
                    int balita = input.nextInt();
                    jumlahBalita += (float)balita; //Perhitungan akan diubah kebentuk float
                    break;
                default:
                    System.out.print("Pilihlah opsi yang tertera antara angka 1 sampai 5 saja!");
            }
            do {
                System.out.print("\nMasih ingin menambah (y/n) : ");
                ulangMenu = input.next().charAt(0);
                if (ulangMenu != 'n' && ulangMenu != 'y') {
                    System.out.println("Maaf inputan tidak sesuai");
                }
            } while(ulangMenu != 'n' && ulangMenu != 'y'); //Perulangan untuk menginput lebih dari 1 case
        } while(ulangMenu != 'n' && ulangMenu == 'y'); //Perulangan untuk kembali ke do yang memuat usia sampai while bernilai False lanjut ke bawah

        //Menjumlahkan keseluruhan orang
        float jumlahOrang = jumlahLaki + jumlahPerempuan + jumlahRemaja + jumlahAnak + jumlahBalita;

        //Kondisi apabila jumlah ganjil lebih dari 5 maka perempuan nambah 1 porsi
        float porsiPerempuan;
        if (jumlahOrang > 5.0F && jumlahOrang % 2.0F == 1.0F) {
            porsiPerempuan = jumlahPerempuan * 2.0F;
        } else {
            porsiPerempuan = jumlahPerempuan * 1.0F;
        }

        //Perhitungan masing-masing usia
        float porsiLaki = jumlahLaki * 2.0F;
        float porsiAnak = jumlahAnak * 1.0F / 2.0F;
        float porsiBalita = jumlahBalita * 1.0F;
        float porsiRemaja = jumlahRemaja * 1.0F;

        //HItung jumlah keseluruhan porsi
        float totalPorsi = porsiLaki + porsiPerempuan + porsiAnak + porsiBalita + porsiRemaja;

        //Cetak Output
        System.out.println("Rentang Usia :");
        System.out.println("\nJumlah laki-laki dewasa: " + (int)jumlahLaki);
        System.out.println("Jumlah perempuan dewasa: " + (int)jumlahPerempuan);
        System.out.println("Jumlah remaja: " + (int)jumlahRemaja);
        System.out.println("Jumlah anak-anak: " + (int)jumlahAnak);
        System.out.println("Jumlah balita: " + (int)jumlahBalita);
        System.out.println();
        System.out.println("Jumlah orang dan porsi yang diperoleh :");
        System.out.println("Jumlah Keseluruhan Orang: " + (int)jumlahOrang);
        System.out.println("Jumlah Keseluruhan Porsi: " + totalPorsi);
    }
}
