package logic05;

import java.util.Scanner;

public class Soal08_FibodanPrima {
    public static void main(String[] args) {
        //Input panjang deret
        Scanner input = new Scanner(System.in);
        System.out.print("Input panjang deret = ");
        int batasan = input.nextInt();

        //Definisikan variabel
        int fibonaci = 1;
        int bilAwal = 0;
        int bilAkhir = 1;
        int helper = 0;

        //Definisikan array
        int[] array = new int[batasan];
        int[] arrayFibonaci = new int[batasan];
        int[] arrayPrima = new int[batasan];
        int[] arraySum = new int[batasan];

        //Deret Fibonaci
        int i;
        for(i = 0; i < array.length; ++i) {
            System.out.print(fibonaci + " ");
            arrayFibonaci[i] = fibonaci;
            fibonaci = bilAwal + bilAkhir;
            bilAwal = bilAkhir;
            bilAkhir = fibonaci;
        }
        System.out.println();

        //Proses mencari bilangan prima dan penjumlahan fibo+prima
        i = 1;
        while(true) {
            int number = 0;
            for(int j = 1; j <= i; ++j) {
                if (i % j == 0) { // i=1 akan di moduluskan dengan j yang isinya = 1,2,3,...i
                    ++number; // number harus 2 karena prima dapat dibagi nilainya sendiri dan angka 1
                }
            }
            if (number == 2) { //Karena dia hanya dapat dibagi dirinya sendiri dan satu
                arrayPrima[helper] = i; //Menyimpan nilai prima
                ++helper;
                System.out.print(i + " ");
                if (helper == array.length) { //Nilai helper harus sama dengan panjang array lenght untuk masuk perhitungan 2 bilangan
                    System.out.println();
                    System.out.println();

                    for(i = 0; i < array.length; ++i) {
                        arraySum[i] = arrayFibonaci[i] + arrayPrima[i]; //Menghitung penjumlahan indeks fibo+prima
                        System.out.print(arraySum[i] + " ");
                    }
                    return; //Kalau helper belum memenuhi panjang array lanjut ke i++
                }
            }
            ++i;
        }
    }
}
