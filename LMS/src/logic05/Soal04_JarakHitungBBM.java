package logic05;

import java.util.Scanner;

public class Soal04_JarakHitungBBM {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        //Cetak Rute
        System.out.println("Menghitung Penggunaan BBM");
        System.out.println("1. Toko");
        System.out.println("2. Tempat 1");
        System.out.println("3. Tempat 2");
        System.out.println("4. tempat 3");
        System.out.println("5. Tempat 4");

        //Masukkan banyaknya rute
        System.out.println("Masukkan nomer rute yang dituju (Ex: 1-2-2-1): ");
        String urutan = input.nextLine();

        String[] rute = urutan.split("-");
        int[] arrayRute = new int[rute.length];

        for (int i = 0; i < rute.length; i++) {
            arrayRute[i] = Integer.parseInt(rute[i]);
        }
        int jarak = 0;
        //Membuat perulangan dan kondisi tiap rute
        for(int i = 0; i < arrayRute.length - 1; ++i) {
            if (arrayRute[i] == 1 && arrayRute[i + 1] == 2) {
                jarak += 2000;
            } else if (arrayRute[i] == 1 && arrayRute[i + 1] == 3) {
                jarak += 2500;
            } else if (arrayRute[i] == 1 && arrayRute[i + 1] == 4) {
                jarak += 4000;
            } else if (arrayRute[i] == 1 && arrayRute[i + 1] == 5) {
                jarak += 6500;
            } else if (arrayRute[i] == 2 && arrayRute[i + 1] == 1) {
                jarak += 500;
            } else if (arrayRute[i] == 2 && arrayRute[i + 1] == 3) {
                jarak += 500;
            } else if (arrayRute[i] == 2 && arrayRute[i + 1] == 4) {
                jarak += 2000;
            } else if (arrayRute[i] == 2 && arrayRute[i + 1] == 5) {
                jarak += 4500;
            } else if (arrayRute[i] == 3 && arrayRute[i + 1] == 1) {
                jarak += 2500;
            } else if (arrayRute[i] == 3 && arrayRute[i + 1] == 2) {
                jarak += 1500;
            } else if (arrayRute[i] == 3 && arrayRute[i + 1] == 4) {
                jarak += 1500;
            } else if (arrayRute[i] == 3 && arrayRute[i + 1] == 5) {
                jarak += 4000;
            } else if (arrayRute[i] == 4 && arrayRute[i + 1] == 1) {
                jarak += 4000;
            } else if (arrayRute[i] == 4 && arrayRute[i + 1] == 2) {
                jarak += 2000;
            } else if (arrayRute[i] == 4 && arrayRute[i + 1] == 3) {
                jarak += 1500;
            } else if (arrayRute[i] == 4 && arrayRute[i + 1] == 5) {
                jarak += 2500;
            } else if (arrayRute[i] == 5 && arrayRute[i + 1] == 1) {
                jarak += 6500;
            } else if (arrayRute[i] == 5 && arrayRute[i + 1] == 2) {
                jarak += 4500;
            } else if (arrayRute[i] == 5 && arrayRute[i + 1] == 3) {
                jarak += 4000;
            } else if (arrayRute[i] == 5 && arrayRute[i + 1] == 4) {
                jarak += 2500;
            }
        }
        // Menghitung pengeluaran BBM berdasarkan jarak yang ditempuh
        System.out.println("Output : ");
        System.out.println("BBM = " + jarak / 2500 + " liter");
    }
}

