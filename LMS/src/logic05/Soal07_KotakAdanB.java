package logic05;

import java.util.Random;
import java.util.Scanner;

public class Soal07_KotakAdanB {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        //Input jumlah kartu/gambar
        System.out.print("Masukkan jumlah kartu atau gambaran anda = ");
        int kartu = input.nextInt();

        while(kartu > 0) { //Cek kondisi kartu harus lebih dari 0
            System.out.print("Masukkan jumlah tawaran anda = ");

            int tawaran;
            for(tawaran = input.nextInt(); tawaran > kartu; tawaran = input.nextInt()) { //Jika tawaran melebihi kartu di tangan maka loopin untuk memasukkan sejumlah kartu atau kurang dari sejumlah kartu ditangan
                System.out.println("Kartu anda melebihi batas kartu");
                System.out.println("Masukkan jumlah tawaran anda = ");
            }

            //Definisi variabel int = 0
            int kotaka = 0;
            int kotakb = 0;
            int pilihan = 0;
            int lawan = 0;

            //Definisikan string kosong
            String pesan = " ";
            String menyerah = " ";

            System.out.println();

            //Membuat variabel random yang memuat angka random 1-10
            Random random = new Random();
            int random1 = random.nextInt(10);
            int random2 = random.nextInt(10);

            //Input pilihan kotak
            System.out.print("Pilihan Kotak  A atau B ? : ");
            char kotak = input.next().charAt(0);

            System.out.println();
            //Proses memilh kotak
            if (kotak == 'B' || kotak == 'b') { //Kondisi memilih kotak B
                kotakb = random2;
                pilihan = random2;
                lawan = random1;
                kotaka = random1;
            } else if(kotak == 'A' || kotak == 'a'){ //Kondisi memilih kotak A
                kotaka = random1;
                pilihan = random1;
                lawan = random2;
                kotakb = random2;
            }else{
                System.out.println("Hanya ada kotak A dan B ");
            }

            if (pilihan < lawan) { //Kondisi kartu lawan lebih besar dari kartu kita
                kartu -= tawaran; //kartu ditangan dikurangi tawaran
                pesan = "You Lose";
            } else if (pilihan > lawan) { //Kondisi kartu kita lebih besar dari kartu lawan
                kartu += tawaran; //kartu ditangan dijumlah tawaran
                pesan = "You Win";
            } else {
                System.out.println("Draw"); //Kondisi jika kartu draw
            }
            //Cetak hasil kartu tiap kotak
            System.out.println("nilai kotak A = " + kotaka + " ,kotak B = " + kotakb + " ,kartu anda sekarang = " + kartu);

            System.out.println(pesan);

            System.out.println();
            if (kartu >= 0) { //Kondisi apabila kartu masih ada lanjut main atau ngak
                System.out.println("Kartu Anda Saat ini : " + kartu);
                System.out.print("Anda Menyerah? (y/n) : ");
                input.nextLine();
                menyerah = input.nextLine();
            } else {
                System.out.println("Kartu Habis / Anda Kalah");
            }
            if (menyerah.equals("y") || menyerah.equals("Y")) { //Kondisi ketika menyerah
                break;
            }
        }
    }
}
