package logic05;

import java.util.Scanner;

public class Soal09_NinjaHatori {
    public static void main(String[] args) {

        //Cetak Output
        System.out.println("Keterangan pola ninja hatori : ");
        System.out.println("N = naik, T = turun");
        System.out.println("");
        System.out.print("Masukkan pola : ");
        Scanner input = new Scanner(System.in);
        String pola = input.nextLine(); //Kurang spasi

        //Membuat arrayPola
        char[] arrayPola = pola.toCharArray();


        //Definisikan variabel
        int mdpl = 0;
        int gunung = 0;
        int lembah = 0;

        int i;
        for(i = 0; i < arrayPola.length; i++) {
            if (arrayPola[i] == 'N') { //Apabila terdapat N dalam array maka mdpl nambah
                mdpl++;
                if (mdpl == 0) { //Karena untuk mencapai 1 gunung di mulai dengan N kemudian T jika NT terhitung 1 gunung
                    lembah++;
                }
            } else if (arrayPola[i] == 'T') {//Apabila terdapat T dalam array maka mdpl berkurang
                mdpl--;
                if (mdpl == 0) { //Karena untuk mencapai 1 lembah di mulai dengan T kemudian N jika TN terhitung 1 lembah
                    gunung++;
                }
            }
        }
        for(i = 0; i < arrayPola.length; i++) {
            System.out.print(arrayPola[i] + " ");
        }
        System.out.println("\n");
        System.out.println("Jumlah Gunung " + gunung);
        System.out.println("Jumlah Lembah " + lembah);
    }
}
