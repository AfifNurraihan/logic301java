package logic05;

import java.util.Scanner;

public class Soal01_AngkaGanjilGenap {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);

        System.out.println("Masukkan nilai n : ");
        int numberInt = input.nextInt();

        System.out.println("Output : ");
        for (int i = 1; i <= numberInt ; i++) {
            if (i%2!=0) {
                System.out.print(" "+i);
            }
        }
        System.out.println();
        for (int i = 1; i <= numberInt; i++) {
            if(i%2==0){
                System.out.print(" "+i);
            }
        }
    }
}
