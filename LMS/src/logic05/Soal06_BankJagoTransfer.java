package logic05;

import java.util.Scanner;

public class Soal06_BankJagoTransfer {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        //Definsikan pin dan saldo awal
        String pin = "123456";
        long saldo = 0L;

        //Proses Setor Tunai + Transfer
        char ulangTransaksi;
        do {
            System.out.print("Masukan PIN = ");
            String Pin = input.nextLine();
            if (pin.equals(Pin)) { //Kondisi cek pin apakah sesuai atau tidak dengan pendefinisian pin
                //Memasukkan nominal setor uang
                System.out.print("Masukan nominal setor tunai \nRp ");
                long SetorTunai = (long)input.nextInt();

                //Saldo bertambah sesuai nominal
                saldo += SetorTunai;

                //Output jika transaksi berhasil
                System.out.println("Setor Tunai Berhasil");

                //Menampilkan saldo terkini
                System.out.println("Saldo anda saat ini Rp " + saldo);
                System.out.println();

                //Cetak pilihan transaksi selanjutnya yaitu Transfer
                System.out.println("           Transfer :          ");
                System.out.println("1.Antar Rekening \t2.Antar Bank");

                //Input pilihan jenis transfer
                System.out.print("Pilih Jenis Transfer : ");
                int menuTransfer = input.nextInt();
                if (menuTransfer == 1) { //Kondisi transfer antar rekening
                    System.out.println("Transfer Sesama Bank");
                    System.out.println("Masukan nomor rekening tujuan anda");
                    long noRek1 = input.nextLong(); //Input nomer rekening
                    System.out.print("Masukan nominal transfer Rp ");
                    long nominalTransfer = input.nextLong(); //Input nominal transfer
                    if (nominalTransfer >= saldo) { //Kondisi apabila saldo tidak mencukupi untuk transfer
                        System.out.println("Saldo tidak mencukupi");
                    } else if (nominalTransfer <= saldo) { //Kondisi apabila mencukupi untuk transfer
                        System.out.println("Transfer ke nomor rekening : " + noRek1);
                        System.out.print("Jumlah nominal transfer Rp " + nominalTransfer);
                        saldo -= nominalTransfer; //Mengurangi saldo sebelumnya dengan nominal yang ditransferkan
                        System.out.println();
                        System.out.print("Transaksi berhasil, Saldo anda saat ini Rp " + saldo);
                    }
                } else if (menuTransfer == 2) { //Kondisi transfer antar bank
                    System.out.println("Masukan kode bank");
                    int kodeBank = input.nextInt(); //Masukkan kode bank
                    System.out.println("Masukan nomor rekening tujuan anda");
                    long noRek1 = input.nextLong(); //Masukkan nomer rekening
                    System.out.print("Masukan nominal transfer Rp ");
                    long nominalTransfer = input.nextLong();
                    if (nominalTransfer + 7500L >= saldo) { //Kondisi apabila saldo kurang dari nominal transfer + biaya admin tidak mencukupi
                        System.out.println("Saldo tidak mencukupi");
                    } else if (nominalTransfer <= saldo + 7500L) { //Kondisi apabila saldo cukup apabila dari nominal transfer + biaya admin
                        System.out.println("Tranfer ke nomor rekening : " + noRek1);
                        System.out.print("Jumlah nominal transfer Rp " + nominalTransfer);
                        saldo -= nominalTransfer + 7500L; //Hitung saldo yang dikurangi nominal transfer + admin
                        System.out.println();
                        System.out.print("Transaksi berhasil,Saldo anda saat ini Rp "+saldo);
                    }
                }
            } else { //Kondisi apabila inputan PIN salah
                System.out.println("PIN yang anda masukan salah");
            }
            System.out.println();
            System.out.print("\nTransaksi lagi? (y/n) : ");
            ulangTransaksi = input.next().charAt(0);
        } while(ulangTransaksi == 'y');//Perulangan apabilai ingin melakukan transaksi lagi
        System.out.println("Terimakasih Telah Bertransaksi, Selamat Beraktivitas");
    }
}
