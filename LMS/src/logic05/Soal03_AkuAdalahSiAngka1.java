package logic05;

import java.util.Scanner;

public class Soal03_AkuAdalahSiAngka1 {
    static int hitungPenjumlahanKuadrat(int simpenangka) {
        int hasilsum = 0;

        //Ubah  ke string
        String numberString = Integer.toString(simpenangka);

        //Ubah numberString ke charArray
        char[] charArray = numberString.toCharArray();

        //Array kosong
        int[] digitArray = new int[charArray.length];

        //Ubah nilai String Angka ke nilai sebenarnya
        for(int i = 0; i < charArray.length; i++) {
            digitArray[i] = Character.getNumericValue(charArray[i]);
        }

        //Menghitung jumlah kuadrat dari tiap digit pada digitArray
        for(int i = 0; i < digitArray.length; i++) {
            hasilsum = (int)(hasilsum + Math.pow(digitArray[i],2));
        }
        //Mengembalikan nilai hasilsum
        return hasilsum;
    }
    public static void main(String[] args) {
        System.out.print("Masukan nilai n : ");
        Scanner input = new Scanner(System.in);
        int inputNumber = input.nextInt();
        int deret = 100;
        int helper = 0;

        while(true) {
            //Simpan nilai deret pada tampung
            int tampungAngka = deret;

            //Dilakukan dahulu melalui kondisi do kemudian while
            //nilai tampung angka memenuhi kondisi False akan dikembalikan ke kondisi do hingga bernilai True
            do {
                tampungAngka = hitungPenjumlahanKuadrat(tampungAngka);
            } while(tampungAngka >= 10);

            //Jika penjumlahan pada fungsi hitungPenjumlahanKuadrat
            // mereturn hasilsum=1 maka cetak output
            if (tampungAngka == 1) {
                helper++;
                System.out.println(deret+" adalah \"Si Angka 1\"");
            }
            //Menghitung nilai yang hasil penjumlahan kuadrat 1
            // hingga jumlah ditemukan sebanyak input number
            if (helper == inputNumber) {
                break;
            }
            //Deret akan bertambah setiap kali while berlangsung
            deret++;
        }
    }
}
