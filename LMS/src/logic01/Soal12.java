package logic01;
import java.util.Scanner;
public class Soal12 {
    public static void main(String[] args) {
        Scanner input = new Scanner(System.in);
        // No 12

        // Prompt input
        System.out.print("Input value of n: ");
        int n = input.nextInt();

        //Prerequisite
        int helper=2;
        int hitung=1;
        boolean prima;

        // Enter the value to array variable
        while (hitung <= n)
        {
            prima = false;
            if (helper==2 || helper==3 || helper== 5)
            {
                prima = true;
            }
            else if (helper%2 !=0)
            {
                if (helper%3 !=0)
                {
                    if(helper%5 !=0)
                    {
                        prima = true;
                    }
                }
            }
            if (prima == true){
                System.out.print(helper+" ");
                hitung++;
            }
            helper++;
        }
        System.out.println();
        input.close();
    }
}
