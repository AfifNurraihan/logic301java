package logic03;

import java.util.Scanner;

public class Soal08_Birthdaycakecandles {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the number of sequence: ");
        String numberString = input.nextLine();

        String[] numberStringArray = numberString.split(" ");
        int[] numberInt = new int[numberStringArray.length];
        int hightcandles = 1;
        int blown = 0;

        for (int i = 0; i < numberInt.length; i++) {
            numberInt[i] = Integer.parseInt(numberStringArray[i]);
            if(numberInt[i] > hightcandles) {
                hightcandles = numberInt[i];
                blown = 1;
            }
            else if(numberInt[i] == hightcandles) {
                blown++;
            }
        }
        System.out.println("number of candles blown : "+ blown);
    }
}