package logic03;

import java.util.Scanner;

public class Soal09_Verybigsum {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the number of sequence: ");
        String numberString = input.nextLine();

        String[] numberStringArray = numberString.split(" ");
        int[] numberLong = new int[numberStringArray.length];

        for (int i = 0; i < numberLong.length; i++) {
            numberLong[i] = Integer.parseInt(numberStringArray[i]);
        }

        //Output
        long helper = 0;
        System.out.println("Output : ");
        for (int i = 0; i < numberLong.length ; i++) {
            helper += numberLong[i];
        }
        System.out.println(helper);
    }
}
