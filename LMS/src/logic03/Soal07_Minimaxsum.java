package logic03;

import java.util.Scanner;

public class Soal07_Minimaxsum {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the number of sequence: ");
        String numberString = input.nextLine();

        String[] numberStringArray = numberString.split(" ");
        int[] numberInt = new int[numberStringArray.length];
        int answer = 0;

        //value of array, number of sequence
        for (int i = 0; i < numberInt.length; i++) {
            numberInt[i] = Integer.parseInt(numberStringArray[i]);
        }

        System.out.print("Sum! Except: ");
        int numberEx = input.nextInt();

        int helper=0;
        for (int i = 0; i < numberInt.length; i++) {
            if(numberEx == numberInt[0]){
                helper = (answer += numberInt[i]) - numberInt[0];
            }
            else if(numberEx == numberInt[1]){
                helper = (answer += numberInt[i]) - numberInt[1];
            }
            else if(numberEx == numberInt[2]){
                helper += numberInt[i] - numberInt[2];
            }
            else if(numberEx == numberInt[3]) {
                helper = (answer += numberInt[i]) - numberInt[3];
            }
            else if(numberEx == numberInt[4]) {
                helper = (answer += numberInt[i]) - numberInt[4];
            }
            else{
                answer += numberInt[i];
            }
        }
        System.out.print("The answer is : ");
        System.out.println(helper);
    }
}
