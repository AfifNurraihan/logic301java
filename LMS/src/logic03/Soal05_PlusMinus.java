package logic03;

import java.util.Scanner;

public class Soal05_PlusMinus {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        // Input the value
        System.out.println("Enter the number sequence: ");
        String[] numberString = input.nextLine().split(" ");
        int[] numberInt = new int[numberString.length];

        // Change type StringArray to IntArray
        for (int i = 0; i < numberInt.length; i++) {
            numberInt[i] = Integer.parseInt(numberString[i]);
        }
        int positive = 0;
        int negative = 0;
        int neutral= 0;

        //Procces Count Plus and Minus
        for (int i = 0; i < numberInt.length; i++) {
            if(numberInt[i]>0){
                positive+=1;
            }else if(numberInt[i]<0){
                negative+=1;
            }else{
                neutral+=1;
            }
        }
        // Output
        float a = (float) positive/numberInt.length;
        float b = (float) negative/numberInt.length;
        float c = (float) neutral/numberInt.length;
        System.out.println();
        System.out.println("Output : ");
        System.out.println(a);
        System.out.println(b);
        System.out.println(c);
    }
}
