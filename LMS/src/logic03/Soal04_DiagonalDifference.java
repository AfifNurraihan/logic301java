package logic03;

import java.util.Scanner;

public class Soal04_DiagonalDifference {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        // Input the dimension of matrix
        System.out.println("Enter the dimension of matrix: ");
        int numberdim = input.nextInt();

        System.out.println();
        System.out.println("Input the value of matrix");
        // Arrray matrix
        int[][] matrix = new int[numberdim][numberdim];
        int diag1=0,diag2=0;

        //Create matrix
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                System.out.print("matrix[" + i + "]" + "[" + j + "] = ");
                matrix [i][j] = input.nextInt();
            }
        }
        //Print matrix
        System.out.println();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[0].length; j++) {
                if(i==j){
                    diag1+=matrix[i][j];
                }
                if((j+i)==(numberdim-1)){
                    diag2+=matrix[i][j];
                }
            }
        }
        System.out.println("Output : ");
        System.out.println(Math.abs(diag1-diag2));
    }
}
