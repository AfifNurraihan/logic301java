package logic03;

import java.util.Scanner;

public class Soal06_Staircase {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        // Input the value
        System.out.println("Enter the number sequence: ");
        int n = input.nextInt();

        // Prerequisite
        String[][] array2dstring = new String[n][n];

        System.out.println("Output :");

        for (int i = 0; i < array2dstring.length; i++) {
            for (int j = 0; j < array2dstring.length; j++) {
                if ((i+j) == (n-1)){
                    System.out.print(array2dstring[i][j] = "#");
                }else if((i+j)>(n-1)){
                    System.out.print(array2dstring[i][j] = "#");
                }
                else{
                    System.out.print(array2dstring[i][j]= " ");
                }
            }System.out.println();
        }
    }
}
