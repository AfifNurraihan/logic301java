package logic03;

import java.util.Scanner;

public class Soal12_Modus {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);

        System.out.println("Enter the number sequence: ");
        String numberString = input.nextLine();
        String[] numberStringArray = numberString.split(" ");
        double[] angka = new double[numberStringArray.length];

        for (int i = 0; i < angka.length; i++) {
            angka[i] = Integer.parseInt(numberStringArray[i]);
        }

        for (int i = 0; i < 9; i++) {
            for (int j = 0; j < 9 - i; j++) {
                if (angka[j] > angka[j + 1]) {
                    double n = angka[j];
                    angka[j] = angka[j + 1];
                    angka[j + 1] = n;}
            }
        }
        double jumlahangka = 0;
        double jumlahmodus = 0;
        double modus = 0;
        for (int i = 0; i < 10; i++) {
            for (int j = 0; j < 10; j++) {
                if (angka[j] == angka[i] && j != i) {
                    jumlahangka++;}}
            if (jumlahangka >= jumlahmodus) {
                jumlahmodus = jumlahangka;
                modus = angka[i];
                jumlahangka = 0;}}
        System.out.println("Modus  : " + modus);
    }
}
