package logic03;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Scanner input = new Scanner(System.in);

        String answer = "Y";

        while(answer.toUpperCase().equals("Y"))
        {
            System.out.println("Please enter the question number: ");
            int number = input.nextInt();

            switch (number)
            {
                case 1:
                    Soal01_SolveMeFirst.Resolve();
                    break;
                case 3:
                    Soal03_SimpleArraySum.Resolve();
                    break;
                case 4:
                    Soal04_DiagonalDifference.Resolve();
                    break;
                case 5:
                    Soal05_PlusMinus.Resolve();
                    break;
                case 6:
                    Soal06_Staircase.Resolve();
                    break;
                case 7:
                    Soal07_Minimaxsum.Resolve();
                    break;
                case 8:
                    Soal08_Birthdaycakecandles.Resolve();
                    break;
                case 9:
                    Soal09_Verybigsum.Resolve();
                    break;
                case 10:
                    Soal10_Comparethetriplets.Resolve();
                    break;
                case 11:
                    Soal11_Median.Resolve();
                    break;
                case 12:
                    Soal12_Modus.Resolve();
                    break;
                default:
                    System.out.println("No question available");
                    break;
            }
            System.out.println();
            System.out.println("Again? (Y/N)");
            input.nextLine();
            answer = input.nextLine();
        }

        System.out.println("Thank you. See you soon");

    }
}

