package logic03;
import java.util.Scanner;

public class Soal03_SimpleArraySum {
    public static void Resolve() {
        Scanner input = new Scanner(System.in);
        // Input the value
        System.out.println("Enter the number sequence: ");
        String numberString = input.nextLine();

        //ChangeStringArray to IntegerArray
        String[] numberStringArray = numberString.split(" ");
        double [] numberInt = new double[numberStringArray.length];
        int answer = 0;

        //Proccesed
        for (int i = 0; i < numberInt.length; i++) {
            numberInt[i] = Integer.parseInt(numberStringArray[i]);
        }

        for (int i = 0; i < numberInt.length; i++) {
            answer += numberInt[i];
        }
        //Print Output
        System.out.println("The answer is: " + answer);
    }
}