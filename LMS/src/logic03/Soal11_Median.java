package logic03;

import java.util.Scanner;

public class Soal11_Median {
    public static void Resolve() {
        Scanner Input = new Scanner(System.in);

        System.out.println("Enter the number of sequence: ");
        String numberString = Input.nextLine();

        String[] numberStringArray= numberString.split(" ");
        int[] numberInt = new int[numberStringArray.length];

        for (int i = 0; i < numberInt.length; i++) {
            numberInt[i] = Integer.parseInt(numberStringArray[i]);
        }

        //Median Proccesed
        System.out.println();
        float median = 0.2f;
        int position = numberInt.length / 2;
        if (numberInt.length % 2 == 0) {
            System.out.println("Series of even numbers");
            median = (float) (numberInt[position - 1] + numberInt[position]) / 2;
            System.out.printf("The Median Value is %.2f", median);
        } else {
            System.out.println("Odd number series");
            median = numberInt[position];
            System.out.print("The Median value is " + median);
        }
    }
}
